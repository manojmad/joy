/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import localStyles from './componentStyles.less';

class contentTitle extends Component {
  render() {
    return (
      <div className={localStyles.contentTitle}>
        <h2>
          {this.props.title} <span>{this.props.content}</span>
        </h2>
      </div>
    );
  }
}

export default contentTitle;
