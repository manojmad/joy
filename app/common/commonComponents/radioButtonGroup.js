import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Radio, Card } from 'antd';
import localStyles from '../commonComponents/componentStyles.less';

export default class RadioButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }
  onChange = e => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  };
  render() {
    const { radioBiuttonData } = this.props;
    return (
      <Radio.Group
        onChange={this.onChange}
        value={this.state.value}
        style={{ paddingTop: '40px', display: 'flex' }}
      >
        {radioBiuttonData.map(item => (
          <div>
            <div
              style={{
                paddingRight: 20,
              }}
            >
              <Card className={localStyles.radioCard}>
                {item.cardImage ? (
                  <img src={item.cardImage} alt="software-it image" />
                ) : null}
                {item.tittle ? (
                  <p className={localStyles.cardTitle}>{item.tittle}</p>
                ) : null}
                <Radio value={item.value} />
              </Card>
            </div>
          </div>
        ))}
      </Radio.Group>
    );
  }
}
