import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
// import './index.css';
import { Pagination } from 'antd';

const itemRender = (current, type, originalElement) => {
  if (type === 'prev') {
    return <a href="/">Go to Page |</a>;
  }
  if (type === 'next') {
    return <a href="/">Next</a>;
  }
  return originalElement;
};

class pagination extends Component {
  render() {
    return (
      <div>
        <Pagination
          style={{ textAlign: 'center' }}
          size="small"
          total={500}
          itemRender={itemRender}
        />
      </div>
    );
  }
}

export default pagination;
