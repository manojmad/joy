import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Table, Icon } from 'antd';
import DeleteFunction from './deleteFunction';
import EditFunction from './editFunction';

class TableComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  // To get filterdata
  getData = dataValues => {
    this.props.getFilterData(dataValues);
  };

  render() {
    const columnData = this.props.columnData;
    columnData.push({
      title: 'Actions',
      dataIndex: 'action',
      key: 'x',
      width: '140px',
      render: (text, record, index) => (
        <div style={{ display: 'flex' }}>
          <EditFunction
            index={index}
            selectedRecord={record}
            getEditData={this.props.getEditData}
            fieldName={this.props.fieldName}
          />
          <DeleteFunction
            tableListData={this.props.tableListData}
            selectedRecord={record}
            getData={this.getData}
          />
        </div>
      ),
    });
    return (
      <div>
        <Table
          columns={columnData}
          dataSource={this.props.tableListData}
          pagination={false}
          size="small"
        />
      </div>
    );
  }
}
export default TableComponent;
