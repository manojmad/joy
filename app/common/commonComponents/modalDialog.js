import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Button, Icon, Modal, Form } from 'antd';
import localStyles from '../commonComponents/componentStyles.less';

class ModalBox extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
    };
  }

  componentDidMount() {
    this.props.setClick(v => this.setModalVisible(v));
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.modalVisible) {
      this.setState({ modalVisible: nextProps.modalVisible });
    }
  }

  // To show ModalDialog
  setModalVisible = (modalVisible, actionType) => {
    this.setState({ modalVisible }, () => {
      if (actionType === 'cancel') {
        this.props.resetModel();
        this.props.handleReset();
      }
    });
  };
  render() {
    return (
      <div>
        <Button
          type="primary"
          shape="circle"
          // className={localStyles.buttonSave}
          onClick={() => this.setModalVisible(true)}
        >
          <Icon type="plus" className={localStyles.plusIcon} />
        </Button>

        <Modal
          width={this.props.width}
          title={this.props.title}
          centered
          closable={false}
          visible={this.state.modalVisible}
          footer={[
            <div style={{ padding: '40px 0 10px 0' }}>
              <Button
                key="cancel"
                onClick={() => this.setModalVisible(false, 'cancel')}
              >
                Cancel
              </Button>
              <Button
                className={localStyles.buttonSave}
                key="submit"
                htmlType="submit"
                form={this.props.form}
              >
                Save
              </Button>
            </div>,
          ]}
        >
          {this.props.fieldContent}
        </Modal>
      </div>
    );
  }
}

export default Form.create()(ModalBox);
