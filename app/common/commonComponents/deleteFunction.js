import React from 'react';
import 'antd/dist/antd.css';
import { Icon, Popconfirm } from 'antd';
import Notification from './notification';

class DeleteFunction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleDelete = () => {
    const values = this.props.tableListData.filter(
      row => row.id !== this.props.selectedRecord.id,
    );
    this.props.getData(values);
    Notification('error', 'Deleted Successfully');
  };
  render() {
    return (
      <div>
        <Popconfirm
          title="Are you sure to delete this Record"
          onConfirm={() => this.handleDelete()}
        >
          <Icon type="delete" style={{ cursor: 'pointer' }} />
        </Popconfirm>
      </div>
    );
  }
}
export default DeleteFunction;
