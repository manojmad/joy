import React, { Component } from 'react';
import { Form, Button } from 'antd';
import Styles from '../commonComponents/componentStyles.less';

class InputForm extends Component {
  
  // to cancel the form submission
  handleCancel = () => {
    this.props.toggleModel(false);
    this.props.handleReset();
  };

  render() {
    return (
      <div>
        {this.props.formContent}
        <Button
          type="ghost"
          onClick={() => {
            this.handleCancel();
          }}
        >
          Cancel
        </Button>
        <Button
          className={Styles.buttonSave}
          type="primary"
          htmlType="submit"
          form={this.props.form}
        >
          Save
        </Button>
      </div>
    );
  }
}

export default Form.create()(InputForm);
