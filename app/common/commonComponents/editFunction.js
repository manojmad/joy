import React from 'react';
import 'antd/dist/antd.css';
// import {Popconfirm} from 'antd';
import ModalDialog from './modalDialog';
import { FormItem } from '../../common/AntdConst/AntdConst';
import { Input, Form, Icon } from 'antd';
import Notification from './notification';

// let getsaveData;
class EditFunction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      updatedData: {},
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const savedFormData = values;
        // values.id = this.state.tableData.length + 1;
        this.sendEditData(values);
        this.setModalVisible(false);
        this.props.form.resetFields();
        // this.state.tableData.push(savedFormData);
        Notification('success', 'Updated Successfully');
      }
    });
  };
  render() {
    console.log('selectedRecord', this.props.selectedRecord);
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 34 },
        sm: { span: 28 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 50 },
      },
    };
    const content = (
      <Form onSubmit={this.handleSubmit} id="editsample">
        <FormItem
          {...formItemLayout}
          label="Role Name"
          hasFeedback
        >
          {getFieldDecorator(this.props.fieldName, {
            initialValue: this.props.selectedRecord[this.props.fieldName],
            rules: [
              {
                required: true,
                message: 'Please add Roles Type',
              },
            ],
          })(
            <Input
              type="text"
              style={{ width: '240px' }}
              name="roles"
              placeholder="Roles"
            />,
          )}
        </FormItem>
      </Form>
    );
    return (
      <div>
        <ModalDialog
          width="22%"
          title="Edit"
          form="editsample"
          functionType="edit"
          tableListData={this.props.tableListData}
          formContent={content}
          setClick={click => (this.setModalVisible = click)}
          sendData={click => (this.sendEditData = click)}
          getEditData={this.props.getEditData}
          index={this.props.index}
        />
      </div>
    );
  }
}
export default Form.create()(EditFunction);
