import React from 'react';
import MenuIcon from '../images/extraMenu.png';
import Roles from '../pages/Components/Roles';
import ExpertiseLevel from '../pages/Components/ExpertiseLevel';
import Tools from '../pages/Components/Tools';
import Skill from '../pages/Components/Skill';
import EmployeeType from '../pages/Components/EmployeeType';
import Branch from '../pages/Components/Branch';
import Holidays from '../pages/Components/Holidays';
import OrganizationType from '../pages/OrganizationType';
import EmployeeGroup from '../pages/EmployeeGroup';

const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
export function isUrl(path) {
  return reg.test(path);
}

const menuData = [
  {
    name: 'Dashboard',
    path: 'dashboard',
    // hideInMenu: true,
    component: () => <h1>Dashboard</h1>,
  },
  {
    name: 'Schedules',
    path: 'schedules',
    component: () => <h1>Schedules</h1>,
    // hideInMenu: true,
  },
  {
    name: 'Employees',
    path: 'employees',
    // hideInMenu: true,
    component: () => <h1>Employees</h1>,
  },
  {
    name: 'Attendance',
    path: 'attendance',
    component: () => <h1>Attendance</h1>,
    // hideInMenu: true,
  },
  {
    name: 'Reports',
    path: 'reports',
    component: () => <h1>Reports</h1>,
    // hideInMenu: true,
  },
  {
    name: <img src={MenuIcon} alt="moreMenu" style={{ paddingLeft: '50%' }} />,
    path: 'masterMenu',
    children: [
      {
        name: 'Employee Type',
        path: 'employeeType',
        component: EmployeeType,
      },
      {
        name: 'Roles',
        path: 'roles',
        component: Roles,
      },
      {
        name: 'Skill',
        path: 'skill',
        component: Skill,
      },
      {
        name: 'Expertise Level',
        path: 'expertiseLevel',
        component: ExpertiseLevel,
      },
      {
        name: 'Tools',
        path: 'tools',
        component: Tools,
      },
      {
        name: 'Shifts',
        path: 'shifts',
        component: null,
      },
      {
        name: 'Branch',
        path: 'branch',
        component: Branch,
      },
      {
        name: 'Holidays',
        path: 'holidays',
        component: Holidays,
      },
      {
        name: 'OrganizationType',
        path: 'organizationType',
        component: OrganizationType,
      },
      {
        name: 'EmployeeGroup',
        path: 'employeeGroup',
        component: EmployeeGroup,
      },
    ],
  },
];

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map(item => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(
        item.children,
        `${parentPath}${item.path}/`,
        item.authority,
      );
    }
    return result;
  });
}

export const getMenuData = () => formatter(menuData);
