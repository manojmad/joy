import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Button } from 'antd';
// import { BrowserRouter as Router, Route } from 'react-router-dom';
// import { Link, Switch } from 'react-router-dom';
import ContentTitle from '../../common/commonComponents/contentTitle';
import Software from '../../images/software.png';
import Pharma from '../../images/pharma.png';
import Restaurant from '../../images/restaurant.png';
import Travel from '../../images/travel.png';
import Education from '../../images/education.png';
import Others from '../../images/others.png';
import RadioButtonGroup from '../../common/commonComponents/radioButtonGroup';
import localStyles from '../OrganizationType/organizationType.less';
// import EmployeeGroup from '../EmployeeGroup/employeeGroup';

class SaveData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  
  render() {
    const radioBiuttonData = [
      {
        cardImage: Software,
        tittle: 'Software-IT',
        value: 1,
      },
      {
        cardImage: Pharma,
        tittle: 'Pharma',
        value: 2,
      },
      {
        cardImage: Restaurant,
        tittle: 'Restaurant',
        value: 3,
      },
      {
        cardImage: Travel,
        tittle: 'Travel',
        value: 4,
      },
      {
        cardImage: Education,
        tittle: 'Education',
        value: 5,
      },
      {
        cardImage: Others,
        tittle: 'Others',
        value: 6,
      },
    ];
    return (
      <div style={{ height: '84vh' }}>
        <ContentTitle title="Organization" />
        <div className={localStyles.organizationcontent}>
          <div className={localStyles.organizationTypecontentDiv}>
            <h3>Organization Type</h3>
            <RadioButtonGroup radioBiuttonData={radioBiuttonData} />
          </div>
          <Button
            className={localStyles.buttonNext}
            type="primary"
            htmlType="submit"
          >
            Next
          </Button>
        </div>
      </div>
    );
  }
}

export default SaveData;

