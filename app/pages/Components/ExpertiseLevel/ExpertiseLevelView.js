/* eslint-disable no-return-assign */
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Layout, Form } from 'antd';

import ContentTitle from '../../../common/commonComponents/contentTitle';

import SampleTable from './ExpertiseLevelTable';
import SampleForm from './ExpertiseLevelForm';
import * as localConsts from './ExpertiseLevelConsts';

const { Content } = Layout;

class ExpertiseLevelView extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentAction: 'close',
      toggleTable: false,
      toggleForm: false,
      modalVisible: false,
    };
  }

  componentDidMount() {
    //  call data table List from reducer
    this.props.dispatch({
      type: `${localConsts.NAMESPACE}/actionList`,
      payload: {},
    });
  }

  render() {
    const { currentAction, toggleTable, toggleForm } = this.state;

    return (
      <Fragment>
        <Layout style={{ height: '84vh' }}>
          <ContentTitle title={localConsts.PAGE_TITLE} />
          <Content
            style={{
              height: '74vh',
              // maxHeight: '65vh',
              // overflow: 'hidden',
              padding: '20px 50px',
            }}
          >
            <div hidden={toggleTable}>
              <SampleTable
                currentAction={currentAction}
                toggleView={this.toggleView}
                toggleEdit={this.toggleEdit}
                getRecord={this.getRecord}
              />
            </div>
            <div hidden={toggleForm}>
              <SampleForm
                currentAction={currentAction}
                toggleEdit={this.toggleEdit}
                modalVisible={this.state.modalVisible}
                resetModel={this.resetModel}
                toggleAdd={this.toggleAdd}
              />
            </div>
          </Content>
        </Layout>
      </Fragment>
    );
  }
  resetModel = () => {
    this.setState({
      currentAction: 'close',
      modalVisible: false,
    });
  };
  toggleEdit = () => {
    this.setState({
      currentAction: 'edit',
      modalVisible: true,
    });
  };

  toggleView = () => {
    this.setState({
      currentAction: 'view',
      toggleTable: true,
      toggleForm: false,
    });
  };
}

export default Form.create()(
  connect(({ expertiseLevel }) => ({ expertiseLevel }))(ExpertiseLevelView),
);
