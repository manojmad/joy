/* eslint-disable no-param-reassign */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ModalDialog from '../../../common/commonComponents/modalDialog';
import { FormItem } from '../../../common/AntdConst/AntdConst';
import { Form, Input } from 'antd';
import * as localConsts from './ShiftsConsts';
import ColorPickerPalette from '../../../common/commonComponents/colorPicker';
import TimePickerPalette from '../../../common/commonComponents/timepicker2';

const newObject = {
  id: '',
  name: '',
};

class ShiftsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: newObject,
      visible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentAction === 'edit') {
      this.setState({ data: nextProps.dataById });
    } else {
      this.setState({ data: newObject });
    }
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((errors, values) => {
      if (!errors) {
        // this.props.initialValue;
        values.colorPicker = this.state.colorbg;
        values.timePicker = this.state.schedule;
        this.props.dispatch({
          type: `${localConsts.NAMESPACE}/actionSave`,
          payload: values,
        });
        this.setModalVisible(false);
        this.props.form.resetFields();
        this.props.resetModel();
        //  this.props.formReset();
      }
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };
  handleChangeTime = (startTime, endTime, splitStartTime, splitEndTime) => {
    this.setState({
      schedule:
        splitStartTime === ' '
          ? `${startTime} to ${endTime}`
        `${startTime} to ${endTime} - ${splitStartTime} to ${splitEndTime}`,
    });
    console.log('rajeshwari', splitStartTime, splitEndTime);
  };
  handleChangeColor = colorValue => {
    console.log('colorrgfgtgvt', colorValue);
    this.setState({
      colorbg: colorValue,
    });
    console.log('colorbg', this.state.colorbg);
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 34 },
        sm: { span: 28 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 50 },
      },
    };
    const { currentAction } = this.props;
    const { data } = this.state;
    const formContent = (
      <Form onSubmit={this.handleSubmit} id="shiftsView">
        <div style={{ display: 'flex' }}>
          <FormItem {...formItemLayout} label="Shift Name" hasFeedback>
            {getFieldDecorator('shiftName', {
              initialValue: data.shiftName,
              rules: [
                {
                  required: true,
                  message: 'Please add Shift Name',
                },
              ],
            })(
              <Input
                type="text"
                style={{ width: '240px' }}
                name="shiftName"
                placeholder="Shift Name"
              />,
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Default Color"
            hasFeedback
            style={{ paddingLeft: '128px' }}
          >
            {getFieldDecorator('colorPicker', {
              initialValue: '',
            })(
              <ColorPickerPalette handleChangeColor={this.handleChangeColor} />,
            )}
          </FormItem>
        </div>
        <FormItem {...formItemLayout} hasFeedback>
          {getFieldDecorator('timePicker', {
            initialValue: data.TimePickerPalette,
          })(<TimePickerPalette handleChangeTime={this.handleChangeTime} />)}
        </FormItem>
      </Form>
    );
    return (
      <Fragment>
        <ModalDialog
          width="40%"
          title="Add Shifts"
          fieldContent={formContent}
          form="shiftsView"
          handleReset={this.handleReset}
          modalVisible={this.props.modalVisible}
          resetModel={this.props.resetModel}
          setClick={click => (this.setModalVisible = click)}
        />
      </Fragment>
    );
  }
}

ShiftsForm.propTypes = {
  dataById: PropTypes.any,
  form: PropTypes.object,
  currentAction: PropTypes.string,
};

export default Form.create()(
  connect(({ shifts }) => ({
    dataById: shifts.reducerById || {},
    // dataById: roles.reducerById,
  }))(ShiftsForm),
);
