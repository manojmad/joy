import {
  serviceSave,
  serviceDelete,
  serviceList,
  serviceById,
} from './ShiftsService';
import { NAMESPACE } from './ShiftsConsts';

export default {
  namespace: NAMESPACE,

  state: {
    reducerSave: [],
    reducerDelete: [],
    reducerList: [],
    reducerById: [],
  },

  effects: {
    *actionSave({ payload }, { call, put }) {
      console.log('handleSubmit Data:', payload);
      // const response = yield call(serviceSave, payload);
      // yield put({
      //   type: 'reducerSave',
      //   payload: response.data || [],
      // });
      // const respo = yield call(serviceList, {});
      // yield put({
      //   type: 'reducerList',
      //   payload: respo.data || [],
      // });
      // return response.data.message || [];
    },

    *actionDelete({ payload }, { call, put }) {
      console.log('Table Delete Data:', payload);
      // const response = yield call(serviceDelete, payload);
      // yield put({
      //   type: 'reducerDelete',
      //   payload: response.data || [],
      // });
      // const respo = yield call(serviceList, {});
      // yield put({
      //   type: 'reducerList',
      //   payload: respo.data || [],
      // });
      // return response.data.message || [];
    },

    *actionList({ payload }, { call, put }) {
      // /const response = yield call(serviceList, payload);
      yield put({
        type: 'reducerList',
        payload: [
          {
            id: 1,
            shiftName: 'First Shift',
            starttime: '9:00am',
            endtime: '5:00am',
          },
          {
            id: 2,
            shiftName: 'Second Shift',
            starttime: '2:00pm',
            endtime: '10:00pm',
          },
          {
            id: 3,
            shiftName: 'Third Shift',
            starttime: '9:00pm',
            endtime: '5:00pm',
          },
          {
            id: 4,
            shiftName: 'Split Shift',
            // shiftTime: '9:00am to 1:00pm-5:00pm to 9:00pm',
            starttime: '9:00am to 1:00pm-',
            endtime: '5:00pm to 9:00pm',
          },
        ],
      });
    },

    *actionById({ payload }, { call, put }) {
      // const response = yield call(serviceById, payload);
      yield put({
        type: 'reducerById',
        payload,
      });
      return payload;
    },
  },

  reducers: {
    reducerSave(state, action) {
      return {
        ...state,
        reducerSave: action.payload,
      };
    },
    reducerDelete(state, action) {
      return {
        ...state,
        reducerDelete: action.payload,
      };
    },

    reducerList(state, action) {
      return {
        ...state,
        reducerList: action.payload,
      };
    },
    reducerById(state, action) {
      return {
        ...state,
        reducerById: action.payload,
      };
    },
  },
};
