import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Icon, Popconfirm } from 'antd';
import * as localConsts from './ShiftsConsts';

class ShiftsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableListData: [],
      EditmodalVisible: false,
    };
  }

  handleAction = (data, actionType) => {
    if (actionType === 'delete') {
      this.props.dispatch({
        type: `${localConsts.NAMESPACE}/actionDelete`,
        payload: data,
      });
    } else if (actionType === 'edit') {
      this.props.dispatch({
        type: `${localConsts.NAMESPACE}/actionById`,
        payload: data,
      });
      this.props.toggleEdit();
    } else if (actionType === 'view') {
    }
  };
  render() {
    const { data } = this.props;
    return (
      <div>
        <Table
          rowKey="id"
          columns={this.tableColumns}
          dataSource={data}
          size="small"
        />
      </div>
    );
  }

  tableColumns = [
    {
      title: 'Shift Name',
      dataIndex: 'shiftName',
      key: 'shiftName',
    },
    {
      title: 'Shift Times',
      dataIndex: `{$('starttime')$('endtime')$('duration')}`,
      key: 'shiftTime',
      render: (text, record, index) => (
        // console.log('record', record);
        <div>
          {record.starttime}
          {record.endtime}
          {/* {record.duration} */}
        </div>
      ),
    },
    {
      title: 'Actions',
      width: 100,
      fixed: 'right',
      render: data => (
        <div style={{ display: 'flex' }}>
          <Icon
            type="edit"
            onClick={() => {
              this.handleAction(data, 'edit');
            }}
          />
          <Popconfirm
            title="Are you sure ？"
            icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
            onConfirm={() => this.handleAction(data, 'delete')}
          >
            <Icon
              type="delete"
              style={{ cursor: 'pointer', paddingLeft: '7px' }}
            />
          </Popconfirm>
        </div>
      ),
    },
  ];
}
ShiftsTable.propTypes = {
  data: PropTypes.any,
  toggleEdit: PropTypes.func,
  toggleView: PropTypes.func,
  currentAction: PropTypes.any,
};

export default connect(({ shifts }) => ({
  data: shifts.reducerList || [],
}))(ShiftsTable);
