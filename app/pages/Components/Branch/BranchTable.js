import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Icon, Popconfirm } from 'antd';
import * as localConsts from './BranchConsts';

class BranchTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableListData: [],
      EditmodalVisible: false,
    };
  }

  handleAction = (data, actionType) => {
    if (actionType === 'delete') {
      this.props.dispatch({
        type: `${localConsts.NAMESPACE}/actionDelete`,
        payload: data,
      });
    } else if (actionType === 'edit') {
      this.props.dispatch({
        type: `${localConsts.NAMESPACE}/actionById`,
        payload: data,
      });
      this.props.toggleEdit();
    } else if (actionType === 'view') {
    }
  };

  // handleEdit = data => {};
  render() {
    const { data } = this.props;
    return (
      <div>
        <Table
          rowKey="id"
          columns={this.tableColumns}
          dataSource={data}
          size="small"
        />
      </div>
    );
  }

  tableColumns = [
    {
      title: 'Name',
      dataIndex: 'branch',
      key: 'branch',
    },
    {
      title: 'Address',
      dataIndex:`{$('StreetLine1')$('StreetLine1')$('city')}`,
      key: 'address',
      render: (text, record, index) => 
        //console.log('record', record);
         (
          <div>
            {record.StreetLine1}
            {record.StreetLine2}
            {record.city}
          </div>
        )
      ,
    },
    {
      title: 'Phone',
      dataIndex: 'Phone',
      key: 'Phone',
    },
    {
      title: 'Email',
      dataIndex: 'Email',
      key: 'Email',
    },
    {
      title: 'Actions',
      width: 100,
      fixed: 'right',
      render: data => (
        <div style={{ display: 'flex' }}>
          <Icon
            type="edit"
            onClick={() => {
              this.handleAction(data, 'edit');
            }}
          />
          <Popconfirm
            title="Are you sure ？"
            icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
            onConfirm={() => this.handleAction(data, 'delete')}
          >
            <Icon
              type="delete"
              style={{ cursor: 'pointer', paddingLeft: '7px' }}
            />
          </Popconfirm>
        </div>
      ),
    },
  ];
}
BranchTable.propTypes = {
  data: PropTypes.any,
  toggleEdit: PropTypes.func,
  toggleView: PropTypes.func,
  currentAction: PropTypes.any,
};

export default connect(({ branch }) => ({
  data: branch.reducerList || [],
}))(BranchTable);
