/* eslint-disable no-param-reassign */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ModalDialog from '../../../common/commonComponents/modalDialog';
import { FormItem } from '../../../common/AntdConst/AntdConst';
import { Form, Input } from 'antd';
import * as localConsts from './BranchConsts';

const newObject = {
  id: '',
  name: '',
};

class BranchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: newObject,
      visible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentAction === 'edit') {
      this.setState({ data: nextProps.dataById });
    } else {
      this.setState({ data: newObject });
    }
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((errors, values) => {
      if (!errors) {
          this.props.dispatch({
            type: `${localConsts.NAMESPACE}/actionSave`,
            payload: values,
          });
        this.setModalVisible(false);
          this.props.form.resetFields();
        this.props.resetModel();
        }
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 34 },
        sm: { span: 28 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 50 },
      },
    };
    const { currentAction } = this.props;
    const { data } = this.state;
    const formContent = (
      <Form onSubmit={this.handleSubmit} id="branchView">
        <div style={{ display: 'flex' }}>
          <FormItem {...formItemLayout} label="Branch Name" hasFeedback>
            {getFieldDecorator('branch', {
              initialValue: data.branch,
              rules: [
                {
                  required: true,
                  message: 'Please add Branch Name',
                },
              ],
            })(
              <Input
                type="text"
                style={{ width: '200px' }}
                name="Branch"
                placeholder="Branch Name"
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Street Line1" hasFeedback>
            {getFieldDecorator('StreetLine1', {
              initialValue: data.StreetLine1,
              rules: [
                {
                  required: true,
                  message: 'Please add Street Line1',
                },
              ],
            })(
              <Input
                type="text"
                style={{ width: '200px' }}
                name="Address"
                placeholder="Street Line1"
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Street Line2" hasFeedback>
            {getFieldDecorator('StreetLine2', {
              initialValue: data.StreetLine2,
              rules: [
                {
                  required: true,
                  message: 'Please add Street Line2',
                },
              ],
            })(
              <Input
                type="text"
                style={{ width: '200px' }}
                name="Street Line2"
                placeholder="Street Line2"
              />,
            )}
          </FormItem>
        </div>
        <FormItem
          {...formItemLayout}
          label="Country/State/City/Zip or PIN"
          hasFeedback
        >
          {getFieldDecorator('city', {
            initialValue: data.city,
            rules: [
              {
                required: true,
                message: 'Please add Country/State/City/Zip or PIN',
              },
            ],
          })(
            <Input
              type="text"
              style={{ width: '200px' }}
              name="Address"
              placeholder="Country/State/City/Zip or PIN"
            />,
          )}
        </FormItem>
        <h2>Contact Info</h2>

        <div style={{ display: 'inline-flex' }}>
          <FormItem {...formItemLayout} label="Phone" hasFeedback>
            {getFieldDecorator('Phone', {
              initialValue: data.Phone,
              rules: [
                {
                  required: true,
                  message: 'Please enter valid Phone number',
                },
              ],
            })(
              <Input
                type="number"
                style={{ width: '200px' }}
                name="Phone"
                placeholder="Phone"
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Email" hasFeedback>
            {getFieldDecorator('Email', {
              initialValue: data.Email,
              rules: [
                {
                  required: true,
                  message: 'Please enter Email',
                },
              ],
            })(
              <Input
                type="text"
                style={{ width: '200px' }}
                name="Email"
                placeholder="Email"
              />,
            )}
          </FormItem>
        </div>
      </Form>
    );
    return (
      <Fragment>
        <ModalDialog
          width="60%"
          title="Add Branch"
          fieldContent={formContent}
          form="branchView"
          handleReset={this.handleReset}
          modalVisible={this.props.modalVisible}
          resetModel={this.props.resetModel}
          setClick={click => (this.setModalVisible = click)}
        />
      </Fragment>
    );
  }
}

BranchForm.propTypes = {
  dataById: PropTypes.any,
  form: PropTypes.object,
  currentAction: PropTypes.string,
};

export default Form.create()(
  connect(({ branch }) => ({
    dataById: branch.reducerById || {},
    // dataById: roles.reducerById,
  }))(BranchForm),
);
