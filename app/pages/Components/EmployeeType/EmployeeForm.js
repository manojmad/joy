/* eslint-disable no-param-reassign */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ModalDialog from '../../../common/commonComponents/modalDialog';
import { FormItem } from '../../../common/AntdConst/AntdConst';
import { Form, Input } from 'antd';
import * as localConsts from './EmployeeConsts';

const newObject = {
  id: '',
  employeeType: '',
};

class EmployeeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: newObject,
      visible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    // console.log('hhjh', nextProps.reducerList);
    if (nextProps.currentAction === 'edit') {
      this.setState({ data: nextProps.dataById });
    } else {
      this.setState({ data: newObject });
    }
  }
  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((errors, values) => {
      if (!errors) {
        this.props.dispatch({
          type: `${localConsts.NAMESPACE}/actionSave`,
          payload: values,
        });
        this.setModalVisible(false);
        this.props.resetModel();
        this.props.form.resetFields();
      }

      //
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 34 },
        sm: { span: 28 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 50 },
      },
    };
    const { currentAction } = this.props;
    const { data } = this.state;
    const formContent = (
      <Form onSubmit={this.handleSubmit} id="employeeView">
        <FormItem
          {...formItemLayout}
          label="Type"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('employeeType', {
            rules: [
              {
                required: true,
                message: 'Please add Employee Type',
              },
            ],
            initialValue: data.employeeType,
          })(
            <Input
              type="text"
              style={{ width: '240px' }}
              name="Employee Type"
              placeholder="Employee Type"
            />,
          )}
        </FormItem>
      </Form>
    );
    return (
      <Fragment>
        <ModalDialog
          width="22%"
          title="Add Employee Type"
          fieldContent={formContent}
          form="employeeView"
          handleReset={this.handleReset}
          modalVisible={this.props.modalVisible}
          resetModel={this.props.resetModel}
          setClick={click => (this.setModalVisible = click)}
        />
      </Fragment>
    );
  }
}

EmployeeForm.propTypes = {
  dataById: PropTypes.any,
  form: PropTypes.object,
  currentAction: PropTypes.string,
};

export default Form.create()(
  connect(({ employeeType }) => ({
    dataById: employeeType.reducerById || {},
    // dataById: roles.reducerById,
  }))(EmployeeForm),
);
