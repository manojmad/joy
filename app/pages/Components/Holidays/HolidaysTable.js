import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Icon, Popconfirm } from 'antd';
import * as localConsts from './HolidaysConsts';

class HolidaysTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableListData: [],
      EditmodalVisible: false,
    };
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log('frehyg', nextProps);
  //   this.setState({
  //     tableListData: nextProps.data,
  //   });
  // }

  handleAction = (data, actionType) => {
    if (actionType === 'delete') {
      this.props.dispatch({
        type: `${localConsts.NAMESPACE}/actionDelete`,
        payload: data,
      });
    } else if (actionType === 'edit') {
      this.props.dispatch({
        type: `${localConsts.NAMESPACE}/actionById`,
        payload: data,
      });
      this.props.toggleEdit();
    } else if (actionType === 'view') {
    }
  };
  render() {
    const { data } = this.props;
    return (
      <div>
        <Table
          rowKey="id"
          columns={this.tableColumns}
          dataSource={data}
          size="small"
        />
      </div>
    );
  }

  tableColumns = [
    {
      title: 'Date',
      dataIndex: 'dateType',
      key: 'dateType',
      render: (text, record, index) => <div>{record.date}</div>,
    },
    {
      title: 'Day',
      dataIndex: 'dayType',
      key: 'dayType',
      render: (text, record, index) => <div>{record.day}</div>,
    },
    {
      title: 'Festival',
      dataIndex: 'festivalType',
      key: 'festivalType',
    },
    {
      title: 'Actions',
      width: 100,
      fixed: 'right',
      render: data => (
        <div style={{ display: 'flex' }}>
          <Icon
            type="edit"
            onClick={() => {
              this.handleAction(data, 'edit');
            }}
          />
          <Popconfirm
            title="Are you sure ？"
            icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
            onConfirm={() => this.handleAction(data, 'delete')}
          >
            <Icon
              type="delete"
              style={{ cursor: 'pointer', paddingLeft: '7px' }}
            />
          </Popconfirm>
        </div>
      ),
    },
  ];
}
HolidaysTable.propTypes = {
  data: PropTypes.any,
  toggleEdit: PropTypes.func,
  toggleView: PropTypes.func,
  currentAction: PropTypes.any,
};

export default connect(({ holidays }) => ({
  data: holidays.reducerList || [],
}))(HolidaysTable);
