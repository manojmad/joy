/* eslint-disable no-param-reassign */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ModalDialog from '../../../common/commonComponents/modalDialog';
import { FormItem } from '../../../common/AntdConst/AntdConst';
import { Form, Input, DatePicker } from 'antd';
import * as localConsts from './HolidaysConsts';
import moment from 'moment';
// import localStyles from './Holidays.less';

const newObject = {
  id: '',
  festivalType: '',
};

class HolidaysForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: newObject,
      visible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentAction === 'edit') {
      this.setState({ data: nextProps.dataById });
    } else {
      this.setState({ data: newObject });
    }
  }
  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((errors, values) => {
      console.log('Holidays', values);
      if (!errors) {
        // const fieldsValue = {
        //   ...values,
        //   'date': fieldsValue['date'].format('YYYY-MM-DD')}
        this.props.dispatch({
          type: `${localConsts.NAMESPACE}/actionSave`,
          payload: values,
        });
        this.setModalVisible(false);
        this.props.resetModel();
        this.props.form.resetFields();
      }

      //
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 34 },
        sm: { span: 28 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 50 },
      },
    };
    const { currentAction } = this.props;
    const { data } = this.state;
    const formContent = (
      <Form onSubmit={this.handleSubmit} id="festivalView">
        <div style={{ display: 'flex' }}>
          <FormItem
            {...formItemLayout}
            label="Festival Name"
            hasFeedback
            //   style={{ marginLeft: 40 }}
          >
            {getFieldDecorator('festivalType', {
              rules: [
                {
                  required: true,
                  message: 'Please add Festival Type',
                },
              ],
              initialValue: data.festivalType,
            })(
              <Input
                type="text"
                style={{ width: '240px' }}
                name="festival Name"
                placeholder="Festival Name"
              />,
            )}
          </FormItem>

          <Form.Item label="Date">
            {getFieldDecorator('date', { initialValue: moment(data.date) })(
              <DatePicker />,
            )}
          </Form.Item>
        </div>
      </Form>
    );
    return (
      <Fragment>
        <ModalDialog
          width="42%"
          title="Add Holidays"
          fieldContent={formContent}
          form="festivalView"
          handleReset={this.handleReset}
          modalVisible={this.props.modalVisible}
          resetModel={this.props.resetModel}
          setClick={click => (this.setModalVisible = click)}
        />
      </Fragment>
    );
  }
}

HolidaysForm.propTypes = {
  dataById: PropTypes.any,
  form: PropTypes.object,
  currentAction: PropTypes.string,
};

export default Form.create()(
  connect(({ holidays }) => ({
    dataById: holidays.reducerById || {},
    // dataById: roles.reducerById,
  }))(HolidaysForm),
);
