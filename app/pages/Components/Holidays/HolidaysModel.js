import {
  serviceSave,
  serviceDelete,
  serviceList,
  serviceById,
} from './HolidaysService';
import { NAMESPACE } from './HolidaysConsts';
import moment from 'moment';

export default {
  namespace: NAMESPACE,

  state: {
    reducerSave: [],
    reducerDelete: [],
    reducerList: [],
    reducerById: [],
  },

  effects: {
    *actionSave({ payload }, { call, put }) {
      console.log('handleSubmit Data:', payload);
      // const response = yield call(serviceSave, payload);
      // yield put({
      //   type: 'reducerSave',
      //   payload: response.data || [],
      // });
      // const respo = yield call(serviceList, {});
      // yield put({
      //   type: 'reducerList',
      //   payload: respo.data || [],
      // });
      // return response.data.message || [];
    },

    *actionDelete({ payload }, { call, put }) {
      console.log('Table Delete Data:', payload);
      // const response = yield call(serviceDelete, payload);
      // yield put({
      //   type: 'reducerDelete',
      //   payload: response.data || [],
      // });
      // const respo = yield call(serviceList, {});
      // yield put({
      //   type: 'reducerList',
      //   payload: respo.data || [],
      // });
      // return response.data.message || [];
    },

    *actionList({ payload }, { call, put }) {
      // /const response = yield call(serviceList, payload);
      yield put({
        type: 'reducerList',
        payload: [
          {
            id: 1,
            date: moment('2020-01-01').format(' D MMM YYYY'),
            day: moment('2020-01-01').format('dddd'),
            festivalType: "New Year's Day",
          },
          {
            id: 2,
            date: moment('2020-01-26').format(' D MMM YYYY'),
            day: moment('2020-01-26').format('dddd'),
            festivalType: 'Republic Day',
          },
          {
            id: 3,
            date: moment('2020-01-01').format(' D MMM YYYY'),
            day: moment('2020-01-01').format('dddd'),
            festivalType: 'Maha Shivaratri',
          },
        ],
      });
    },

    *actionById({ payload }, { call, put }) {
      // const response = yield call(serviceById, payload);
      yield put({
        type: 'reducerById',
        payload,
      });
      return payload;
    },
  },

  reducers: {
    reducerSave(state, action) {
      return {
        ...state,
        reducerSave: action.payload,
      };
    },
    reducerDelete(state, action) {
      return {
        ...state,
        reducerDelete: action.payload,
      };
    },
    reducerList(state, action) {
      return {
        ...state,
        reducerList: action.payload,
      };
    },
    reducerById(state, action) {
      return {
        ...state,
        reducerById: action.payload,
      };
    },
  },
};
