/* eslint-disable no-return-assign */
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Layout, Form } from 'antd';

import ContentTitle from '../../../common/commonComponents/contentTitle';
import HeaderSearch from '../../../components/HeaderSearch';

import HolidaysTable from './HolidaysTable';
import HolidaysForm from './HolidaysForm';
import * as localConsts from './HolidaysConsts';

const { Content } = Layout;

class HolidaysView extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentAction: 'close',
      toggleTable: false,
      toggleForm: false,
      modalVisible: false,
    };
  }

  componentDidMount() {
    //  call data table List from reducer
    this.props.dispatch({
      type: `${localConsts.NAMESPACE}/actionList`,
      payload: {},
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log('nextProps', nextProps.holidays.reducerList);
  }

  // getSearchData = data => {
  //   console.log('dataff', data);
  //   const searchValues = data.filter(searchItem =>
  //     searchItem.festival.includes(data),
  //   );
  //   console.log('searchValues', searchValues);
  //   // this.setState({ tableData: searchValues });
  // };

  render() {
    const { currentAction, toggleTable, toggleForm } = this.state;

    return (
      <Fragment>
        <Layout style={{ height: '84vh' }}>
          <ContentTitle
            title={localConsts.PAGE_TITLE}
            icon={<HeaderSearch getSearchData={this.getSearchData} />}
          />
          <Content
            style={{
              height: '80vh',
              maxHeight: '70vh',
              overflowX: 'hidden',
              overflowY: 'hidden',
              padding: '20px 50px',
            }}
          >
            <div hidden={toggleTable}>
              <HolidaysTable
                currentAction={currentAction}
                toggleView={this.toggleView}
                toggleEdit={this.toggleEdit}
                getRecord={this.getRecord}
              />
            </div>
            <div hidden={toggleForm}>
              <HolidaysForm
                currentAction={currentAction}
                toggleEdit={this.toggleEdit}
                modalVisible={this.state.modalVisible}
                resetModel={this.resetModel}
                toggleAdd={this.toggleAdd}
              />
            </div>
          </Content>
        </Layout>
      </Fragment>
    );
  }
  resetModel = () => {
    this.setState({
      currentAction: 'close',
      modalVisible: false,
    });
  };
  toggleEdit = () => {
    this.setState({
      currentAction: 'edit',
      modalVisible: true,
    });
  };

  toggleView = () => {
    this.setState({
      currentAction: 'view',
      toggleTable: true,
      toggleForm: false,
    });
  };
}

export default Form.create()(
  connect(({ holidays }) => ({ holidays }))(HolidaysView),
);
