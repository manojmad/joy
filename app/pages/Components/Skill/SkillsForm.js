/* eslint-disable no-param-reassign */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ModalDialog from '../../../common/commonComponents/modalDialog';
import { FormItem } from '../../../common/AntdConst/AntdConst';
import { Form, Input } from 'antd';
import * as localConsts from './SkillsConsts';

const newObject = {
  id: '',
  skillType: '',
};

class SkillsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: newObject,
      visible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentAction === 'edit') {
      this.setState({ data: nextProps.dataById });
    } else {
      this.setState({ data: newObject });
    }
  }
  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((errors, values) => {
      if (!errors) {
        this.props.dispatch({
          type: `${localConsts.NAMESPACE}/actionSave`,
          payload: values,
        });
        this.setModalVisible(false);
        this.props.resetModel();
        this.props.form.resetFields();
      }

      //
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 34 },
        sm: { span: 28 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 50 },
      },
    };
    const { currentAction } = this.props;
    const { data } = this.state;
    const formContent = (
      <Form onSubmit={this.handleSubmit} id="skillsView">
        <FormItem
          {...formItemLayout}
          label="Skill Name"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('skillType', {
            rules: [
              {
                required: true,
                message: 'Please add Skills Type',
              },
            ],
            initialValue: data.skillType,
          })(
            <Input
              type="text"
              style={{ width: '240px' }}
              name="Skill Name"
              placeholder="Skill Name"
            />,
          )}
        </FormItem>
      </Form>
    );
    return (
      <Fragment>
        <ModalDialog
          width="22%"
          title="Add Skills"
          fieldContent={formContent}
          form="skillsView"
          handleReset={this.handleReset}
          modalVisible={this.props.modalVisible}
          resetModel={this.props.resetModel}
          setClick={click => (this.setModalVisible = click)}
        />
      </Fragment>
    );
  }
}

SkillsForm.propTypes = {
  dataById: PropTypes.any,
  form: PropTypes.object,
  currentAction: PropTypes.string,
};

export default Form.create()(
  connect(({ skills }) => ({
    dataById: skills.reducerById || {},
    // dataById: roles.reducerById,
  }))(SkillsForm),
);
