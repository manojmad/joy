import axios from 'axios';
import _ from 'lodash';
import { restURL } from '../../../common/rest_urls';
import { NAMESPACE } from './SkillsConsts';

const pageURL = `${restURL}/${NAMESPACE}`;
const saveURL = `${pageURL}/save`;
const deleteURL = `${pageURL}/delete`;
const listURL = `${pageURL}/list`;
const byIdURL = `${pageURL}/id`;

// export const serviceSave = values =>
//   axios
//     .post(`${saveURL}`, values)
//     .then(response => response)
//     .catch(error => error.response);

export const serviceSave = values => axios.post(`${saveURL}`, values);

export const serviceDelete = data =>
  axios.delete(
    `${deleteURL}?id=${data.id}&deleteReason=${data.deleteReason.reason}`,
  );
export const serviceList = criteria => axios.post(`${listURL}`, criteria);
export const serviceById = id => axios.get(`${byIdURL}?id=${id}`);
