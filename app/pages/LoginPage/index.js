import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox,Alert  } from 'antd';
import {loginSubmition} from './Actions';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import styles from './index.less';
import login from '../../images/login.png';
import permission from '../../images/permission.png';

const FormItem = Form.Item;
class LoginForm extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.loginSubmition(values);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    var {Fail} = this.props;
    
    if ( !(this.props.Success.length == 0 )){
      return  <Redirect to="/gui" />
    }

    return (
      <div style={{height: "100vh",display :"flex"}} >
        <div style={{width : "50%", display :"flex", height : "100%" , background :"#90AEFF"}} >
          <img src={login} alt="logo" style={{margin: "auto"}} />
        </div>
        <div style={{ margin : "auto"}} >
          <h1 className={styles.title} >Login</h1>
          <Form onSubmit={this.handleSubmit} className={styles.loginForm}>
            <FormItem style={{ marginBottom: '10px' }}>
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: 'Please enter username!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" style={{ width: "240px" }} />
              )}
            </FormItem>
            <FormItem style={{ marginBottom: '10px' }}>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please enter Password!' }],
              })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" style={{ width: "240px" }} />
              )}
            </FormItem>
            <FormItem className={styles.remember}>
              {getFieldDecorator
                ('remember', { valuePropName: 'checked', initialValue: false, })
                ( <Checkbox >Remember me</Checkbox> )
              }
            </FormItem>
            {/* alert message on login fail */}
            { (Fail == true) ? <Alert message="Entered user-name or password is not matched " type="error" showIcon /> : <span> </span>}
            <Button type="primary" htmlType="submit" className="login-form-button" style={{ width: "240px" }}>
              Log in
            </Button>
          </Form>
          <div style={{marginTop: '32px', display: 'flex' }}>
              <div>
                <img style={{marginRight: '4px' }} src={permission} alt="logo" />
                <Link to="/forgotPassword">Forgot password</Link>
              </div>
              <div style={{marginLeft: '30px' }}>
                <img style={{marginRight: '4px' }} src={permission} alt="logo" />
                <Link to="/askPermission">Request Access</Link>
              </div>
            </div>
        </div>
      </div>
    );
  } 
}


const mapStateToProps = (state) => {
  return {
    Success : state.LoginSuccess,
    Fail : state.LoginFail

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginSubmition : (values) => dispatch(loginSubmition(values))
  }
}
const WrappedLoginForm = Form.create()(connect(mapStateToProps,mapDispatchToProps) (LoginForm));
export default WrappedLoginForm;