import React, { Component } from 'react';
import { Form, Input, Button, Select } from 'antd';
import { connect } from 'react-redux';
import {registerRequest} from './Actions';
import { SelectOption, FormItem } from '../../common/AntdConst/AntdConst.js'
import login from '../../images/login.png';

class RegisterForm extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.registerRequest(values);
      }
    });
  }

  render() {
    const { getFieldDecorator,Success } = this.props.form;
    if ( Success === true ){
      return   <Redirect to="/" />;
    }
    return (
      <div style={{height: "100vh",display :"flex"}} >
        <div style={{width : "55%" , height : "100%" , background :"#90AEFF"}} >
          <img src={login} alt="logo" style={{margin: "auto"}} />
        </div>
        <div style={{ margin : "auto"}} >
          <h1>Register</h1>
          <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator('Name', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(
                <Input placeholder="Name" style={{ width: "240px", borderBottom: "1px solid #d9d9d9"}} />
              )}
            </FormItem>
            
            <FormItem>
              {getFieldDecorator('userId', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(
                <Input  placeholder="User Id" style={{ width: "240px" }} />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('email', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input  type="password" placeholder="Password" style={{ width: "240px" }} />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('telephone', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(
                <Input   type="email" placeholder="Email" style={{ width: "240px" }} />
              )}
            </FormItem>
            <Select  style={{ width: 240 }} placeholder="Permission" >
              <SelectOption value="jack">Jack</SelectOption>
              <SelectOption value="lucy">Lucy</SelectOption>
              <SelectOption value="disabled" disabled>Disabled</SelectOption>
              <SelectOption value="Yiminghe">yiminghe</SelectOption>
            </Select>
            <FormItem>
              <Button type="primary" htmlType="submit" className="login-form-button" style={{ width: "240px" }}>
                Register
              </Button><br/>
            </FormItem>
          </Form>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    Success : state.RegistrationRequestSuccess,
    Fail : state.RegistrationRequestFail
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerRequest : (values) => dispatch(registerRequest(values))
  }
}
const WrappedRegisterForm = Form.create()(connect(mapStateToProps,mapDispatchToProps) (RegisterForm));
export default WrappedRegisterForm ;