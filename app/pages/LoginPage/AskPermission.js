import React, { Fragment, Component } from 'react';
import { Form, Input, Button, Select,Alert } from 'antd';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {permissionRequest} from './Actions';
import askPermission from '../../images/askPermissiom.png';
import success from '../../images/success.png';
import tickMark from '../../images/tickMark.png';
import backArrow from '../../images/backArrow.png';

const FormItem = Form.Item;
class AskPermissionForm extends Component {
    state = {
        redirect : '' ,
        status : ''
        };

    componentWillMount() {
        this.setState({redirect : false,status : false })
        }
    
    componentWillReceiveProps(nextProps) {
        if(nextProps.Success != undefined || nextProps.Success != null){
            this.setState({
            status : true
            });
        }
    }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.permissionRequest(values);
      }
    });
  }

  changeRedirectStatus = () => {
    this.setState({redirect : true});
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const {Success,Fail} = this.props
    
    const PermissionForm = 
        <div style={{height: "100vh",display :"flex"}} >
            <div style={{width : "55%" , height : "100%" , background :"#FE7680"}} >
            <img src={askPermission} alt="logo" style={{margin: "auto"}} />
            </div>
            <div style={{ margin : "auto"}} >
            <h1>Ask Permission !</h1>
            <Form onSubmit={this.handleSubmit} className="login-form">
                <FormItem>
                {getFieldDecorator('email', {
                    rules: [{ required: true, message: 'Please input your Email Address!' }],
                })(
                    <Input   placeholder="Email Address" style={{ width: "240px" }} />
                )}
                </FormItem>
                <Select style={{ width: 240 }}  placeholder="Select Application">
                    <SelectOption value="jack">Jack</SelectOption>
                    <SelectOption value="lucy">Lucy</SelectOption>
                    <SelectOption value="disabled" disabled>Disabled</SelectOption>
                    <SelectOption value="Yiminghe">yiminghe</SelectOption>
                </Select><br/>
                <Select  style={{ width: 240 }} placeholder="Permission" >
                    <SelectOption value="jack">Jack</SelectOption>
                    <SelectOption value="lucy">Lucy</SelectOption>
                    <SelectOption value="disabled" disabled>Disabled</SelectOption>
                    <SelectOption value="Yiminghe">yiminghe</SelectOption>
                </Select>

                 {/* alert message on login fail */}
                 { (Fail == true) ? <Alert message="Entered email already exist" type="error" showIcon /> : <span> </span>}
                
                <FormItem>
                <Button type="primary" htmlType="submit" className="login-form-button" style={{ width: "240px" }}>
                    Submit
                </Button><br/>
                </FormItem>
            </Form>
            </div>
        </div>
    
    const PermissionSuccess =
        <div style={{height: "100vh",display :"flex"}} >
            <div style={{width : "55%" , height : "100%" , background :"#5EE3C6"}} >
            <img src={success} alt="logo" style={{margin: "auto"}} />
            </div>
            <div style={{ margin : "auto"}} >
            <h1>Ask Permission</h1>
            <span style={{color: "#0DA584"}}><img src={tickMark} alt="logo" style={{margin: "auto"}} />You permission request have been submitted successfully. You will receive when it get approved. </span>
            <Button onClick={ this.changeRedirectStatus}><img src={backArrow} alt="logo" style={{margin: "auto" }} /> Login</Button>
            </div>
        </div>
    
    if ( this.state.redirect ){
        return   <Redirect to="/" />;
      }
      
    return (
        <Fragment>
          {(! this.state.status )  ?  PermissionForm : PermissionSuccess }
        </Fragment>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    Success : state.PermissionRequestSuccess,
    Fail : state.PermissionRequestFail
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
     permissionRequest : (values) => dispatch(permissionRequest(values))
  }
}
const WrappedAskPermissionForm = Form.create()(connect(mapStateToProps,mapDispatchToProps) (AskPermissionForm));
export default WrappedAskPermissionForm;