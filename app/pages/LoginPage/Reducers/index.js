

// login reducers
export function LoginSuccess(state=[],action){
    switch(action.type){
        case  "LOGIN_SUCCESS":
            return  (action.data) || [] ;
        default :
            return state ;
    }
}

export function LoginFail(state=[],action){
    switch(action.type){
        case  "LOGIN_FAIL":
            return true;
        default :
            return state ;
    }
}

// forgot password reducers
export function ForgotRequestSuccess(state=[],action){
    switch(action.type){
        case  "FORGOT_REQUEST_SUCCESS":
            return  true  ;
        default :
            return state ;
    }
}

export function ForgotRequestFail(state=[],action){
    switch(action.type){
        case  "FORGOT_REQUEST_FAIL":
            return true;
        default :
            return state ;
    }
}


export function PermissionRequestSuccess(state=[],action){
    switch(action.type){
        case  "PERMISSION_REQUEST_SUCCESS":
            return  true  ;
        default :
            return state ;
    }
}

export function PermissionRequestFail(state=[],action){
    switch(action.type){
        case  "PERMISSION_REQUEST_FAIL":
            return true;
        default :
            return state ;
    }
}

export function RegistrationRequestSuccess(state=[],action){
    switch(action.type){
        case   "REGISTER_REQUEST_SUCCESS":
            return  true  ;
        default :
            return state ;
    }
}

export function RegistrationRequestFail(state=[],action){
    switch(action.type){
        case  "REGISTER_REQUEST_FAIL":
            return true;
        default :
            return state ;
    }
}

