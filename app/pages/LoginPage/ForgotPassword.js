import React, { Fragment, Component } from 'react';
import { Form, Input, Button, Alert } from 'antd';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {forgotPasswordRequest} from './Actions';
import forgotPassword from '../../images/forgotPassword.png';
import success from '../../images/success.png';
import tickMark from '../../images/tickMark.png';
import backArrow from '../../images/backArrow.png';

const FormItem = Form.Item;
class ForgotPasswordForm extends Component {
  state = {
    redirect : '' ,
    status : ''
  };

  componentWillMount() {
    this.setState({redirect : false , status : false})
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      status : true
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.forgotPasswordRequest(values);
      }
    });
  }

  changeRedirectStatus = () => {
    this.setState({redirect : true});
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { Success,Fail } = this.props;
    const ForgotForm = 
        <div style={{height: "100vh",display :"flex"}} >
            <div style={{width : "55%" , height : "100%" , background :"#FA8F4D"}} >
            <img src={forgotPassword} alt="logo" style={{margin: "auto"}} />
            </div>
            <div style={{ margin : "auto"}} >
            <h1>Forgot Password ?</h1>
            <Form onSubmit={this.handleSubmit} className="login-form">
                <FormItem>
                {getFieldDecorator('email', {
                    rules: [{ required: true, message: 'Please enter Email Address!' }],
                })(
                    <Input   placeholder="Email Address" style={{ width: "240px" }} />
                )}
                </FormItem>

                {/* alert message on login fail */}
                { (Fail == true) ? <Alert message="Entered email is not existed " type="error" showIcon /> : <span> </span>}

                <FormItem>
                <Button type="primary" htmlType="submit" className="login-form-button" style={{ width: "240px" }}>
                    Submit
                </Button><br/>
                </FormItem>
            </Form>
            </div>
        </div>
    
    const ForgotSuccess =
        <div style={{height: "100vh",display :"flex"}} >
            <div style={{width : "55%" , height : "100%" , background :"#5EE3C6"}} >
            <img src={success} alt="logo" style={{margin: "auto"}} />
            </div>
            <div style={{ margin : "auto"}} >
            <h1>Forgot Password ?</h1>
              <div style={{color: "#0DA584"}}><img src={tickMark} alt="logo" style={{margin: "auto" }} />Password reset request submitted successfully </div>
              <Button onClick={ this.changeRedirectStatus}><img src={backArrow} alt="logo" style={{margin: "auto" }} /> Login</Button>
            </div>
        </div>

    if ( this.state.redirect ){
      return   <Redirect to="/" />;
    }
    return (
        <Fragment>
           {(! this.state.status) ? ForgotForm : ForgotSuccess }
          
        </Fragment>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    Success : state.ForgotRequestSuccess,
    Fail : state.ForgotRequestFail
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    forgotPasswordRequest : (values) => dispatch(forgotPasswordRequest(values))
  }
}
const WrappedForgotPasswordForm = Form.create()(connect(mapStateToProps,mapDispatchToProps) (ForgotPasswordForm));
export default WrappedForgotPasswordForm;