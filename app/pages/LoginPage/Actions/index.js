import axios from 'axios';

const MOCK = "http://demo1125005.mockable.io/";

export const loginSubmition = (values) => (dispatch) => {
//    axios.post(MOCK + "login",values).then( () => {
//        axios.get(MOCK + "login/get",values).then( (response) => {
            dispatch ({
                type : "LOGIN_SUCCESS",
                data : true //response.data
            });
//        })
//    }).catch(() => {
//        dispatch ({
//            type : "LOGIN_FAIL",
//        })
//    })
}

export const forgotPasswordRequest = (values) => (dispatch) => {
    axios.post(MOCK + "forgotpassword",values).then( () => {
        axios.get(MOCK + "forgotpassword/get").then( (response) =>{
            dispatch({
                type : "FORGOT_REQUEST_SUCCESS"
            })
        }).catch(() => {
            dispatch ({
                type : "FORGOT_REQUEST_FAIL",
            })
        })
    })
}

export const permissionRequest = (value) => (dispatch) => {
    axios.post(MOCK + "permission",value).then( () => {
        axios.get(MOCK + "permission/get").then( (response) =>{

            dispatch({
                type : "PERMISSION_REQUEST_SUCCESS"
            })
        }).catch(() => {
            dispatch ({
                type : "PERMISSION_REQUEST_FAIL",
            })
        })
    })
}

export const registerRequest = (values) => (dispatch) => {
    axios.post(MOCK + "register",values).then( () => {
        axios.get(MOCK + "register/get").then( (response) =>{
            dispatch({
                type : "REGISTER_REQUEST_SUCCESS"
            })
        }).catch(() => {
            dispatch ({
                type : "REGISTER_REQUEST_FAIL",
            })
        })
    })
}
   