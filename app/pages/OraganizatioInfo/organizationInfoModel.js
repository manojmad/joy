import {
  serviceSave,
  serviceDelete,
  serviceList,
  serviceById,
} from './organizationInfoService';
import { NAMESPACE } from './organizationInfoConsts';

export default {
  namespace: NAMESPACE,

  state: {
    reducerSave: [],
    reducerDelete: [],
    reducerList: [],
    reducerById: [],
  },

  effects: {
    *actionSave({ payload }, { call, put }) {
      console.log('handleSubmit Data:', payload);
      // const response = yield call(serviceSave, payload);
      // yield put({
      //   type: 'reducerSave',
      //   payload: response.data || [],
      // });
      // const respo = yield call(serviceList, {});
      // yield put({
      //   type: 'reducerList',
      //   payload: respo.data || [],
      // });
      // return response.data.message || [];
    },

    *actionDelete({ payload }, { call, put }) {
      console.log('Table Delete Data:', payload);
      // const response = yield call(serviceDelete, payload);
      // yield put({
      //   type: 'reducerDelete',
      //   payload: response.data || [],
      // });
      // const respo = yield call(serviceList, {});
      // yield put({
      //   type: 'reducerList',
      //   payload: respo.data || [],
      // });
      // return response.data.message || [];
    },

    *actionList({ payload }, { call, put }) {
      // /const response = yield call(serviceList, payload);
      // yield put({
      //   type: 'reducerList',
      //   payload: [
      //     {
      //       id: 1,
      //       employeeType: 'Trainee',
      //     },
      //     {
      //       id: 2,
      //       employeeType: 'Contract',
      //     },
      //     {
      //       id: 3,
      //       employeeType: 'Permanent',
      //     },
      //   ],
      // });
    },

    *actionById({ payload }, { call, put }) {
      // const response = yield call(serviceById, payload);
      yield put({
        type: 'reducerById',
        payload,
      });
      return payload;
    },
  },

  reducers: {
    reducerSave(state, action) {
      return {
        ...state,
        reducerSave: action.payload,
      };
    },
    reducerDelete(state, action) {
      return {
        ...state,
        reducerDelete: action.payload,
      };
    },
    reducerList(state, action) {
      return {
        ...state,
        reducerList: action.payload,
      };
    },
    reducerById(state, action) {
      return {
        ...state,
        reducerById: action.payload,
      };
    },
  },
};
