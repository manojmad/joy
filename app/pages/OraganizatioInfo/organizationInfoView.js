/* eslint-disable no-return-assign */
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Layout, Form } from 'antd';
import ContentTitle from '../../../common/commonComponents/contentTitle';
import OrganizationInfoForm from './organizationInfoForm';
import * as localConsts from './organizationInfoConsts';
import localStyles from '../../../common/commonComponents/componentStyles.less'

const { Content } = Layout;

class OrganizationInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentAction: 'close',
      toggleTable: false,
      toggleForm: false,
      modalVisible: false,
    };
  }

  componentDidMount() {
    //  call data table List from reducer
    this.props.dispatch({
      type: `${localConsts.NAMESPACE}/actionList`,
      payload: {},
    });
  }

  render() {
    const { currentAction, toggleForm } = this.state;

    return (
      <Fragment>
        <Layout style={{ height: '84vh',}}>
          <ContentTitle title={localConsts.PAGE_TITLE} />
          <Content
            style={{
              height: '65vh',
              //maxHeight: '65vh',
              overflow: 'hidden',
              padding: '20px 50px',
              backgroundColor:'#ffffff',
            }}
          >
            <div hidden={toggleForm}>
              <OrganizationInfoForm
                currentAction={currentAction}
                toggleEdit={this.toggleEdit}
              />
            </div>
          </Content>
        </Layout>
      </Fragment>
    );
  }
  resetModel = () => {
    this.setState({
      currentAction: 'close',
      modalVisible: false,
    });
  };
  toggleEdit = () => {
    this.setState({
      currentAction: 'edit',
      modalVisible: true,
    });
  };

  toggleView = () => {
    this.setState({
      currentAction: 'view',
      toggleTable: true,
      toggleForm: false,
    });
  };
}

export default Form.create()(
  connect(({ organizationInfo }) => ({ organizationInfo }))(OrganizationInfo),
);
