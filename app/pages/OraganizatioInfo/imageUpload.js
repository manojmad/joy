import React from 'react';
import 'antd/dist/antd.css';
import { Upload,message,Button,Form } from 'antd';
import { FormItem } from '../../../common/AntdConst/AntdConst';

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
//   const isLtWidthHigt = file.width === 'image/116px';
//   if (!isLtWidthHigt) {
//     console.log("jjkfgd",file.width);
//     message.error('Image atleast 116px !');
//   }
  return isJpgOrPng && isLt2M;
}

class ImageUpload extends React.Component {
  state = {
    loading: false,
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  render() {
    const { imageUrl } = this.state;
    
    return (
        <div>
        <div style={{display:'flex'}}>
         <div style={{ width: '116px',height:'116px',backgroundColor:'#C4C4C4',borderRadius:'2px' }}>
        <img src={imageUrl}  />
      </div>
      <div style={{marginLeft:'12px'}}>
     <h2>Logo</h2>
     <p>Use image at least 500px by 500px</p>
      <Upload
        name="avatar"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        beforeUpload={beforeUpload}
        onChange={this.handleChange}
      >
       <Button type={this.state.loading ? 'loading' : 'primary'} >Upload Log </Button>
        </Upload>
        </div>
        </div>
      </div>
    );
  }
}

export default ImageUpload;