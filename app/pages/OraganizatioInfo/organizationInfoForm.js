/* eslint-disable no-param-reassign */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { FormItem } from '../../../common/AntdConst/AntdConst';
import { Form, Input,Button, } from 'antd';
import OrganizationIcon from '../../../images/organizarionIcon.png';
import * as localConsts from './organizationInfoConsts';
import ImageUpload from './imageUpload'
const newObject = {
  id: '',
  organizationInfo: '',
};

class OrganizationInfoForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: newObject,
      visible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentAction === 'edit') {
      this.setState({ data: nextProps.dataById });
    } else {
      this.setState({ data: newObject });
    }
  }
  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((errors, values) => {
      console.log("Info",values);
      if (!errors) {
        this.props.dispatch({
          type: `${localConsts.NAMESPACE}/actionSave`,
          payload: values,
        });
        this.props.form.resetFields();
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 34 },
        sm: { span: 28 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 50 },
      },
    };
    const { currentAction } = this.props;
    const { data } = this.state;
    return (
      <Fragment>
      <div style={{display:"flex"}}>
    <div>
<ImageUpload/>

    </div>
       <div style={{paddingLeft:'138px'}}>    
         <Form onSubmit={this.handleSubmit} id="organizationInfo">
         <div style={{display:"flex"}}>
        <FormItem
          {...formItemLayout}
          label="Organization Name"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('organizationName', {
            rules: [
              {
                required: true,
                message: 'Please add Organization Name',
              },
            ],
            initialValue: '',
          })(
            <Input
             prefix={ <img src={OrganizationIcon} alt="OrgHomeIcon"/>}
              type="text"
              style={{ width: '240px' }}
              name="organizationName"
             // placeholder="Organization Name"
            />,
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Mail Id"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('mailId', {
            rules: [
              {
                type: "email",
                message: "The input is not valid E-mail!"
              },
              {
                required: true,
                message: 'Please add Mail Id',
              },
            ],
            initialValue: '',
          })(
            <Input
              type="text"
              //style={{ width: '240px' }}
              name="mailId"
              //placeholder="Mail Id"
            />,
          )}
        </FormItem>
        </div>
        <FormItem
          {...formItemLayout}
          label="Phone"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('phone', {
            rules: [
              {
                required: true,
                message: 'Please add Phone Number',
              },
            ],
            initialValue: '',
          })(
            <Input
              type="text"
              style={{ width: '240px' }}
              name="phone"
              //placeholder="Phone Number"
            />,
          )}
        </FormItem>
        <h3>Address</h3>
        <div style={{display:'flex'}}>
        <FormItem
          {...formItemLayout}
          label="StreetLine1"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('streetLine1', {
            rules: [
              {
                required: true,
                message: 'Please add Street Line1',
              },
            ],
            initialValue: '',
          })(
            <Input
              type="StreetLine1"
              style={{ width: '240px' }}
              name="streetLine1"
              placeholder="streetLine1"
            />,
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="StreetLine2"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('streetLine2', {
            rules: [
              {
                required: true,
                message: 'Please add Street Line2',
              },
            ],
            initialValue: '',
          })(
            <Input
              type="text"
              style={{ width: '240px' }}
              name="streetLine2"
              placeholder="streetLine2"
            />,
          )}
        </FormItem>
        </div>
        <FormItem
          {...formItemLayout}
          label="Country/State/City/Zip or PIN"
          hasFeedback
          //   style={{ marginLeft: 40 }}
        >
          {getFieldDecorator('country', {
            rules: [
              {
                required: true,
                message: 'Please add Country',
              },
            ],
            initialValue: '',
          })(
            <Input
              type="text"
              style={{ width: '240px' }}
              name="country"
              placeholder="country"
            />,
          )}
        </FormItem>
      </Form>
      </div>
      </div>
      </Fragment>
    );
  }
}

OrganizationInfoForm.propTypes = {
  dataById: PropTypes.any,
  form: PropTypes.object,
  currentAction: PropTypes.string,
};

export default Form.create()(
  connect(({ organizationInfo }) => ({
    //dataById: organizationInfo.reducerById || {},
    // dataById: roles.reducerById,
  }))(OrganizationInfoForm),
);
