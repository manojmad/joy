import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Button } from 'antd';
import ContentTitle from '../../common/commonComponents/contentTitle';
import Employee from '../../images/employee.png';
import Employees50 from '../../images/employees50.png';
import Employees100 from '../../images/employees100.png';
import Employees500 from '../../images/employees500.png';
import Employees1000 from '../../images/employees1000.png';
import Employees1000Plus from '../../images/employees1000Plus.png';
import RadioButtonGroup from '../../common/commonComponents/radioButtonGroup';
import localStyles from '../EmployeeGroup/employeeGroup.less';
class SaveData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const radioBiuttonData = [
      {
        cardImage: Employee,
        tittle: '0-10',
        value: 1,
      },
      {
        cardImage: Employees50,
        tittle: '10-50',
        value: 2,
      },
      {
        cardImage: Employees100,
        tittle: '50-100',
        value: 3,
      },
      {
        cardImage: Employees500,
        tittle: '100-500',
        value: 4,
      },
      {
        cardImage: Employees1000,
        tittle: '500-1000',
        value: 5,
      },
      {
        cardImage: Employees1000Plus,
        tittle: '1000+',
        value: 6,
      },
    ];
    return (
      <div style={{ height: '84vh' }}>
        <ContentTitle title="Organization" />
        <div className={localStyles.employeeGroupcontent}>
          <div className={localStyles.employeeGroupcontentDiv}>
            <h3>Number of Employees</h3>
            <RadioButtonGroup radioBiuttonData={radioBiuttonData} />
          </div>
          <Button
            className={localStyles.buttonNext}
            type="primary"
            htmlType="submit"
          >
            Next
          </Button>
          <Button type="ghost" className={localStyles.buttonCancel}>
            Back
          </Button>
        </div>
      </div>
    );
  }
}
export default SaveData;
