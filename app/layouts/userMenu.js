import React from 'react';
import OrganizationType from '../pages/OrganizationType/organizationType';
// import EmployeeGroup from '../pages/EmployeeGroup/employeeGroup';
import User from '../images/user.png';


const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
export function isUrl(path) {
  return reg.test(path);
}

const userMenu = [
  {
    name: <img src={User} alt="userIcon" />,
    path: 'masterMenu',
    children: [
        {
            name: 'Profile',
            path: 'profile',
            // hideInMenu: true,
            component: null,
          },
          {
            name: 'Organization Setup',
            path: 'OrganizationSetup',
            component: OrganizationType,
            // hideInMenu: true,
          },
          {
            name: 'Logout',
            path: 'logout',
            // hideInMenu: true,
            component: null,
          },
    ],
  },
];

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map(item => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(
        item.children,
        `${parentPath}${item.path}/`,
        item.authority,
      );
    }
    return result;
  });
}

export const getData = () => formatter(userMenu);
